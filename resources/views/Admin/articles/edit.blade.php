@extends('Admin.master')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
          <h2>edit article</h2>
        </div>
        <div class="table-responsive">
            @include('Admin.section.errors')
            <form class="form-horizontal" style="padding: 20px;" action="{{ route('articles.update', ['id'=>$article->id] ) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="col-sm-12">
                  <div class="form-groupe">
                      <label for="title" class="control-label">subject</label>
                      <input type="text" class="form-control" name="title" value="{{ $article->title }}" id="title" placeholder="enter article subject">
                  </div>
              </div>

                <div class="col-sm-12">
                  <div class="form-groupe">
                      <label for="description" class="control-label">description</label>
                      <textarea type="text" class="form-control" name="description" id="description" placeholder="enter article description" rows="5">{{ $article->description }}</textarea>
                  </div>
              </div>

                <div class="col-sm-12">
                  <div class="form-groupe">
                      <label for="body" class="control-label">body</label>
                      <textarea rows="5" class="form-control" name="body"  id="body" placeholder="enter article body">{{ $article->body }}</textarea>
                  </div>
              </div>

                <div class="form-group">
                  <div class="col-sm-12">
                      <div class="col-sm-12">
                        <div class="form-groupe">
                            <label for="images" class="control-label">images</label>
                            <input type="file" class="form-control" name="images" id="images" placeholder="enter article images">
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                      <div class="row">
                          @foreach ($article->images['images'] as $key => $image)
                              <div class="col-sm-2">
                                  <label class="control-label">
                                    {{ $key }}
                                    <input type="radio" name="imagesThumb" value="{{ $image }}" {{ $article->images['thumb']== $image ? 'checked' : ''}}>
                                    <a href="{{ $image }}"> <img src="{{ $image }}" width="100%"></a>
                                  </label>
                              </div>
                          @endforeach
                      </div>
                  </div>
                </div>
                <div class="form-group">

                  <div class="col-sm-6">
                      <div class="col-sm-12">
                        <div class="form-groupe">
                            <label for="tags" class="control-label">tags</label>
                            <input type="text" class="form-control" name="tags" value="{{ $article->tags }}" id="tags" placeholder="enter article tags">
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary">send information</button>
                    <!-- <button type="submit" class="btn btn-danger">ارسال</button> -->
                  </div>

                </div>
            </form>
        </div>
    </div>
@endsection
