@extends('Admin.master')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
          <h2>create article</h2>
        </div>
        <div class="table-responsive">
            @include('Admin.section.errors')
            <form class="form-horizontal" style="padding: 20px;" action="{{ route('articles.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="col-sm-12">
                  <div class="form-groupe">
                      <label for="title" class="control-label">subject</label>
                      <input type="text" class="form-control" name="title" value="{{ old('title') }}" id="title" placeholder="enter article subject">
                  </div>
              </div>

                <div class="col-sm-12">
                  <div class="form-groupe">
                      <label for="description" class="control-label">description</label>
                      <textarea type="text" class="form-control" name="description" id="description" placeholder="enter article description" rows="5">{{ old('description') }}</textarea>
                  </div>
              </div>

                <div class="col-sm-12">
                  <div class="form-groupe">
                      <label for="body" class="control-label">body</label>
                      <textarea rows="5" class="form-control" name="body"  id="body" placeholder="enter article body">{{ old('body') }} </textarea>
                  </div>
              </div>

                <div class="form-group">
                  <div class="col-sm-6">
                      <div class="col-sm-12">
                        <div class="form-groupe">
                            <label for="images" class="control-label">images</label>
                            <input type="file" class="form-control" name="images" id="images" placeholder="enter article images">
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="col-sm-12">
                        <div class="form-groupe">
                            <label for="tags" class="control-label">tags</label>
                            <input type="text" class="form-control" name="tags" id="tags" placeholder="enter article tags" value="{{ old('tags') }}">
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary">send information</button>
                    <!-- <button type="submit" class="btn btn-danger">ارسال</button> -->
                  </div>

                </div>
            </form>
        </div>
    </div>
@endsection
